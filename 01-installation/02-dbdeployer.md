# Installation

I'm running `Ubuntu 20.04.2 LTS` in WSL2, there are some strange quirks with this. 

$HOME/bin does not exists, $HOME/.local/bin is advertised as a replacement.  
$HOME/bin works when the folder is created...  
I fucking hate systemd for this kind of shit.  

# Setup

Setup seems easy enough

```bash
kevin~projectsdbdeployer$
$ dbdeployer init
SANDBOX_BINARY /home/kevin/opt/mysql
SANDBOX_HOME   /home/kevin/sandboxes

--------------------------------------------------------------------------------
Directory /home/kevin/opt/mysql ($SANDBOX_BINARY) was created
This directory is the destination for expanded tarballs

--------------------------------------------------------------------------------
Directory /home/kevin/sandboxes ($SANDBOX_HOME) was created
This directory is the destination for deployed sandboxes

--------------------------------------------------------------------------------
# dbdeployer downloads get mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz
....  51 MB
--------------------------------------------------------------------------------
# dbdeployer unpack mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz
Unpacking tarball mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz to $HOME/opt/mysql/8.0.25
Renaming directory /home/kevin/opt/mysql/mysql-8.0.25-linux-glibc2.17-x86_64-minimal to /home/kevin/opt/mysql/8.0.25
--------------------------------------------------------------------------------
dbdeployer versions
Basedir: /home/kevin/opt/mysql
8.0.25
--------------------------------------------------------------------------------
# dbdeployer defaults enable-bash-completion --run-it --remote
  94 kB
Download of file dbdeployer_completion.sh was successful
# completion file: dbdeployer_completion.sh
# Running: sudo cp dbdeployer_completion.sh /etc/bash_completion.d
# File copied to /etc/bash_completion.d/dbdeployer_completion.sh
# Run the command 'source /etc/bash_completion'
```

works like a charm

# Downloading MySQL

Checking which versions are available:  

```bash
kevin~projectsdbdeployer$
$ dbdeployer downloads list
Available tarballs  ()
                              name                                 OS     version     flavor        size   minimal
---------------------------------------------------------------- ------- --------- ------------- -------- ---------
 Percona-Server-8.0.20-11-Linux.x86_64.glibc2.12-minimal.tar.gz   Linux    8.0.20   percona       103 MB   Y
 Percona-Server-8.0.21-12-Linux.x86_64.glibc2.12-minimal.tar.gz   Linux    8.0.21   percona       104 MB   Y
 Percona-Server-8.0.22-13-Linux.x86_64.glibc2.17-minimal.tar.gz   linux    8.0.22   percona       107 MB   Y
 Percona-Server-8.0.23-14-Linux.x86_64.glibc2.17-minimal.tar.gz   Linux    8.0.23   percona       108 MB   Y
 mysql-4.1.22.tar.xz                                              Linux    4.1.22   mysql         4.6 MB   Y
 mysql-5.0.96-linux-x86_64-glibc23.tar.gz                         Linux    5.0.96   mysql         127 MB
 mysql-5.0.96.tar.xz                                              Linux    5.0.96   mysql         5.5 MB   Y
 mysql-5.1.72.tar.xz                                              Linux    5.1.72   mysql          10 MB   Y
 mysql-5.1.73-linux-x86_64-glibc23.tar.gz                         Linux    5.1.73   mysql         134 MB
 mysql-5.5.61-linux-glibc2.12-x86_64.tar.gz                       Linux    5.5.61   mysql         199 MB
 mysql-5.5.61.tar.xz                                              Linux    5.5.61   mysql         6.6 MB   Y
 mysql-5.5.62-linux-glibc2.12-x86_64.tar.gz                       Linux    5.5.62   mysql         199 MB
 mysql-5.5.62.tar.xz                                              Linux    5.5.62   mysql         6.6 MB   Y
 mysql-5.6.43-linux-glibc2.12-x86_64.tar.gz                       Linux    5.6.43   mysql         329 MB
 mysql-5.6.43.tar.xz                                              Linux    5.6.43   mysql         9.0 MB   Y
 mysql-5.6.44-linux-glibc2.12-x86_64.tar.gz                       Linux    5.6.44   mysql         329 MB
 mysql-5.6.44.tar.xz                                              Linux    5.6.44   mysql         9.1 MB   Y
 mysql-5.7.25-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.25   mysql         645 MB
 mysql-5.7.25.tar.xz                                              Linux    5.7.25   mysql          23 MB   Y
 mysql-5.7.26-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.26   mysql         645 MB
 mysql-5.7.26.tar.xz                                              Linux    5.7.26   mysql          23 MB   Y
 mysql-5.7.27-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.27   mysql         645 MB
 mysql-5.7.28-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.28   mysql         725 MB
 mysql-5.7.29-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.29   mysql         665 MB
 mysql-5.7.30-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.30   mysql         660 MB
 mysql-5.7.31-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.31   mysql         376 MB
 mysql-5.7.34-linux-glibc2.12-x86_64.tar.gz                       Linux    5.7.34   mysql         665 MB
 mysql-8.0.13-linux-glibc2.12-x86_64.tar.xz                       Linux    8.0.13   mysql         394 MB
 mysql-8.0.15-linux-glibc2.12-x86_64.tar.xz                       Linux    8.0.15   mysql         376 MB
 mysql-8.0.16-linux-glibc2.12-x86_64.tar.xz                       Linux    8.0.16   mysql         461 MB
 mysql-8.0.16-linux-x86_64-minimal.tar.xz                         Linux    8.0.16   mysql          44 MB   Y
 mysql-8.0.17-linux-glibc2.12-x86_64.tar.xz                       Linux    8.0.17   mysql         480 MB
 mysql-8.0.17-linux-x86_64-minimal.tar.xz                         Linux    8.0.17   mysql          45 MB   Y
 mysql-8.0.18-linux-glibc2.12-x86_64.tar.xz                       Linux    8.0.18   mysql         504 MB
 mysql-8.0.18-linux-x86_64-minimal.tar.xz                         Linux    8.0.18   mysql          48 MB   Y
 mysql-8.0.19-linux-x86_64-minimal.tar.xz                         Linux    8.0.19   mysql          45 MB
 mysql-8.0.20-linux-x86_64-minimal.tar.xz                         Linux    8.0.20   mysql          44 MB   Y
 mysql-8.0.21-linux-glibc2.17-x86_64-minimal.tar.xz               Linux    8.0.21   mysql          48 MB   Y
 mysql-8.0.22-linux-glibc2.17-x86_64-minimal.tar.xz               Linux    8.0.22   mysql          51 MB   Y
 mysql-8.0.23-linux-glibc2.17-x86_64-minimal.tar.xz               Linux    8.0.23   mysql          52 MB   Y
 mysql-8.0.24-linux-glibc2.17-x86_64-minimal.tar.xz               Linux    8.0.24   mysql          51 MB   Y
 mysql-8.0.25-linux-glibc2.12-x86_64.tar.xz                       Linux    8.0.25   mysql         896 MB
 mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz               Linux    8.0.25   mysql          51 MB   Y
 mysql-cluster-8.0.16-dmr-linux-glibc2.12-x86_64.tar.gz           Linux    8.0.16   ndb           1.1 GB
 mysql-cluster-8.0.17-rc-linux-glibc2.12-x86_64.tar.gz            Linux    8.0.17   ndb           1.1 GB
 mysql-cluster-8.0.19-linux-glibc2.12-x86_64.tar.gz               Linux    8.0.19   ndb           1.2 GB
 mysql-cluster-8.0.20-linux-glibc2.12-x86_64.tar.gz               Linux    8.0.20   ndb           1.2 GB
 mysql-cluster-8.0.22-linux-glibc2.12-x86_64.tar.gz               Linux    8.0.22   ndb           1.3 GB
 mysql-cluster-8.0.23-linux-glibc2.12-x86_64.tar.gz               Linux    8.0.23   ndb           1.3 GB
 mysql-cluster-8.0.24-linux-glibc2.12-x86_64.tar.gz               Linux    8.0.24   ndb           1.4 GB
 mysql-cluster-8.0.25-linux-glibc2.12-x86_64.tar.gz               Linux    8.0.25   ndb           1.4 GB
 mysql-cluster-gpl-7.6.10-linux-glibc2.12-x86_64.tar.gz           Linux    7.6.10   ndb           916 MB
 mysql-cluster-gpl-7.6.11-linux-glibc2.12-x86_64.tar.gz           Linux    7.6.11   ndb           916 MB
 mysql-shell-8.0.17-linux-glibc2.12-x86-64bit.tar.gz              Linux    8.0.17   mysql-shell    30 MB
 mysql-shell-8.0.21-linux-glibc2.12-x86-64bit.tar.gz              Linux    8.0.21   shell          43 MB
 mysql-shell-8.0.22-linux-glibc2.12-x86-64bit.tar.gz              Linux    8.0.22   shell          42 MB
 mysql-shell-8.0.23-linux-glibc2.12-x86-64bit.tar.gz              Linux    8.0.23   shell          44 MB
 mysql-shell-8.0.24-linux-glibc2.12-x86-64bit.tar.gz              Linux    8.0.24   shell          42 MB
 mysql-shell-8.0.25-linux-glibc2.12-x86-64bit.tar.gz              Linux    8.0.25   shell          42 MB
 tidb-master-linux-amd64.tar.gz                                   Linux     3.0.0   tidb           26 MB
 ```

Downloading the needed version:

 ```bash
 kevin~projectsdbdeployer$
$ dbdeployer downloads get mysql-8.0.24-linux-glibc2.17-x86_64-minimal.tar.xz
Downloading mysql-8.0.24-linux-glibc2.17-x86_64-minimal.tar.xz
....  51 MB
File /home/kevin/projects/dbdeployer/mysql-8.0.24-linux-glibc2.17-x86_64-minimal.tar.xz downloaded
Checksum matches
 ```

 # Running MySQL in a sandbox

 First extracting the archive:

 ```bash
 kevin~projectsdbdeployer$
$ dbdeployer unpack mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz
Unpacking tarball mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz to $HOME/opt/mysql/8.0.25
.........100.........200...230
Renaming directory /home/kevin/opt/mysql/mysql-8.0.25-linux-glibc2.17-x86_64-minimal to /home/kevin/opt/mysql/8.0.25
 ```

 Next deploying MySQL:

 ```bash
 kevin~projectsdbdeployerERROR$
$ dbdeployer deploy single 8.0.25
error while filling the sandbox definition: missing libraries will prevent MySQL from deploying correctly
client (/home/kevin/opt/mysql/8.0.25/bin/mysql): [      libncurses.so.5 => not found    libtinfo.so.5 => not found]
.
Use --skip-library-check to skip this check
 ```

 Looks like some libs are missing by default on WSL2, gotta install them.  

 ```bash
 kevin~projectsdbdeployer$
$ sudo apt install libncurses5
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  libtinfo5
The following NEW packages will be installed:
  libncurses5 libtinfo5
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 180 kB of archives.
After this operation, 864 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://archive.ubuntu.com/ubuntu focal/universe amd64 libtinfo5 amd64 6.2-0ubuntu2 [83.0 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal/universe amd64 libncurses5 amd64 6.2-0ubuntu2 [96.9 kB]
Fetched 180 kB in 0s (599 kB/s)
Selecting previously unselected package libtinfo5:amd64.
(Reading database ... 58953 files and directories currently installed.)
Preparing to unpack .../libtinfo5_6.2-0ubuntu2_amd64.deb ...
Unpacking libtinfo5:amd64 (6.2-0ubuntu2) ...
Selecting previously unselected package libncurses5:amd64.
Preparing to unpack .../libncurses5_6.2-0ubuntu2_amd64.deb ...
Unpacking libncurses5:amd64 (6.2-0ubuntu2) ...
Setting up libtinfo5:amd64 (6.2-0ubuntu2) ...
Setting up libncurses5:amd64 (6.2-0ubuntu2) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
 ```

Time to deploy MySQL again

```bash
kevin~projectsdbdeployer$
$ dbdeployer deploy single 8.0.25
Database installed in $HOME/sandboxes/msb_8_0_25
run 'dbdeployer usage single' for basic instructions'
cmd:    /home/kevin/sandboxes/msb_8_0_25/start
err:    exit status 1
stdout: ................................................................................................................................................................................... sandbox server not started yet

stderr:
error creating sandbox: 'exit status 1'
```

Oops, looks like the sandboxserver has a problem

```
kevin~sandboxesmsb_8_0_25$
$ ./status
msb_8_0_25 on
```

Strange the status command say everything is running, either the output is wrong or something is seriously broken.

```
kevin~sandboxesmsb_8_0_25ERROR$
$ ./stop
stop /home/kevin/sandboxes/msb_8_0_25
mysqladmin: connect to server at 'localhost' failed
error: 'Access denied for user 'msandbox'@'localhost' (using password: YES)'
Attempting normal termination --- kill -15 6748
SERVER UNRESPONSIVE --- kill -9 6748
```

Yep something with the sandboxserver is seriously broken.  
Time to dig deeper into it at a later time.

After many tries(strace, different OS) and nothing working.  
There seems to be something in my WSL2 that the dbdeployer does not like.

The current workaround is  running dbdeployer inside a dockerized ubuntu.

```bash
kevin~sandboxesmsb_8_0_25$
$ docker run -t -i ubuntu bash
root@014072c1cd18:/# ll
total 56
drwxr-xr-x   1 root root 4096 Jul  6 09:44 ./
drwxr-xr-x   1 root root 4096 Jul  6 09:44 ../
-rwxr-xr-x   1 root root    0 Jul  6 09:44 .dockerenv*
lrwxrwxrwx   1 root root    7 Jun  9 07:27 bin -> usr/bin/
drwxr-xr-x   2 root root 4096 Apr 15  2020 boot/
drwxr-xr-x   5 root root  360 Jul  6 09:44 dev/
drwxr-xr-x   1 root root 4096 Jul  6 09:44 etc/
drwxr-xr-x   2 root root 4096 Apr 15  2020 home/
lrwxrwxrwx   1 root root    7 Jun  9 07:27 lib -> usr/lib/
lrwxrwxrwx   1 root root    9 Jun  9 07:27 lib32 -> usr/lib32/
lrwxrwxrwx   1 root root    9 Jun  9 07:27 lib64 -> usr/lib64/
lrwxrwxrwx   1 root root   10 Jun  9 07:27 libx32 -> usr/libx32/
drwxr-xr-x   2 root root 4096 Jun  9 07:27 media/
drwxr-xr-x   2 root root 4096 Jun  9 07:27 mnt/
drwxr-xr-x   2 root root 4096 Jun  9 07:27 opt/
dr-xr-xr-x 269 root root    0 Jul  6 09:44 proc/
drwx------   2 root root 4096 Jun  9 07:31 root/
drwxr-xr-x   5 root root 4096 Jun  9 07:31 run/
lrwxrwxrwx   1 root root    8 Jun  9 07:27 sbin -> usr/sbin/
drwxr-xr-x   2 root root 4096 Jun  9 07:27 srv/
dr-xr-xr-x  11 root root    0 Jul  6 09:44 sys/
drwxrwxrwt   2 root root 4096 Jun  9 07:31 tmp/
drwxr-xr-x  13 root root 4096 Jun  9 07:27 usr/
drwxr-xr-x  11 root root 4096 Jun  9 07:31 var/
root@014072c1cd18:/# cd ~
root@014072c1cd18:~# l
root@014072c1cd18:~# ll
total 16
drwx------ 2 root root 4096 Jun  9 07:31 ./
drwxr-xr-x 1 root root 4096 Jul  6 09:44 ../
-rw-r--r-- 1 root root 3106 Dec  5  2019 .bashrc
-rw-r--r-- 1 root root  161 Dec  5  2019 .profile
root@014072c1cd18:~# apt update
Get:1 http://archive.ubuntu.com/ubuntu focal InRelease [265 kB]
Get:2 http://security.ubuntu.com/ubuntu focal-security InRelease [114 kB]
Get:3 http://archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]
Get:4 http://archive.ubuntu.com/ubuntu focal-backports InRelease [101 kB]
Get:5 http://archive.ubuntu.com/ubuntu focal/multiverse amd64 Packages [177 kB]
Get:6 http://archive.ubuntu.com/ubuntu focal/main amd64 Packages [1275 kB]
Get:7 http://security.ubuntu.com/ubuntu focal-security/restricted amd64 Packages [368 kB]
Get:8 http://archive.ubuntu.com/ubuntu focal/universe amd64 Packages [11.3 MB]
Get:9 http://security.ubuntu.com/ubuntu focal-security/main amd64 Packages [925 kB]
Get:10 http://archive.ubuntu.com/ubuntu focal/restricted amd64 Packages [33.4 kB]
Get:11 http://archive.ubuntu.com/ubuntu focal-updates/restricted amd64 Packages [416 kB]
Get:12 http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages [1040 kB]
Get:13 http://security.ubuntu.com/ubuntu focal-security/multiverse amd64 Packages [27.6 kB]
Get:14 http://security.ubuntu.com/ubuntu focal-security/universe amd64 Packages [778 kB]
Get:15 http://archive.ubuntu.com/ubuntu focal-updates/multiverse amd64 Packages [32.0 kB]
Get:16 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages [1361 kB]
Get:17 http://archive.ubuntu.com/ubuntu focal-backports/universe amd64 Packages [6315 B]
Setting up libsystemd0:amd64 (245.4-4ubuntu3.7) ...
(Reading database ... 4127 files and directories currently installed.)
Preparing to unpack .../libudev1_245.4-4ubuntu3.7_amd64.deb ...
Unpacking libudev1:amd64 (245.4-4ubuntu3.7) over (245.4-4ubuntu3.6) ...
Setting up libudev1:amd64 (245.4-4ubuntu3.7) ...
(Reading database ... 4127 files and directories currently installed.)
Preparing to unpack .../libapt-pkg6.0_2.0.6_amd64.deb ...
Unpacking libapt-pkg6.0:amd64 (2.0.6) over (2.0.5) ...
Setting up libapt-pkg6.0:amd64 (2.0.6) ...
(Reading database ... 4127 files and directories currently installed.)
Preparing to unpack .../archives/apt_2.0.6_amd64.deb ...
Unpacking apt (2.0.6) over (2.0.5) ...
Setting up apt (2.0.6) ...
(Reading database ... 4127 files and directories currently installed.)
Preparing to unpack .../libnettle7_3.5.1+really3.5.1-2ubuntu0.2_amd64.deb ...
Unpacking libnettle7:amd64 (3.5.1+really3.5.1-2ubuntu0.2) over (3.5.1+really3.5.1-2ubuntu0.1) ...
Setting up libnettle7:amd64 (3.5.1+really3.5.1-2ubuntu0.2) ...
(Reading database ... 4127 files and directories currently installed.)
Preparing to unpack .../libhogweed5_3.5.1+really3.5.1-2ubuntu0.2_amd64.deb ...
Unpacking libhogweed5:amd64 (3.5.1+really3.5.1-2ubuntu0.2) over (3.5.1+really3.5.1-2ubuntu0.1) ...
Setting up libhogweed5:amd64 (3.5.1+really3.5.1-2ubuntu0.2) ...
(Reading database ... 4127 files and directories currently installed.)
Preparing to unpack .../libprocps8_2%3a3.3.16-1ubuntu2.2_amd64.deb ...
Unpacking libprocps8:amd64 (2:3.3.16-1ubuntu2.2) over (2:3.3.16-1ubuntu2.1) ...
Preparing to unpack .../procps_2%3a3.3.16-1ubuntu2.2_amd64.deb ...
Unpacking procps (2:3.3.16-1ubuntu2.2) over (2:3.3.16-1ubuntu2.1) ...
Setting up libprocps8:amd64 (2:3.3.16-1ubuntu2.2) ...
Setting up procps (2:3.3.16-1ubuntu2.2) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
root@014072c1cd18:~# $ curl -s https://raw.githubusercontent.com/datacharmer/dbdeployer/master/scripts/dbdeployer-install.sh | bash
bash: $: command not found
root@014072c1cd18:~# curl -s https://raw.githubusercontent.com/datacharmer/dbdeployer/master/scripts/dbdeployer-install.
sh | bash
bash: curl: command not found
root@014072c1cd18:~# apt install curl
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  ca-certificates krb5-locales libasn1-8-heimdal libbrotli1 libcurl4 libgssapi-krb5-2 libgssapi3-heimdal
  libhcrypto4-heimdal libheimbase1-heimdal libheimntlm0-heimdal libhx509-5-heimdal libk5crypto3 libkeyutils1
  libkrb5-26-heimdal libkrb5-3 libkrb5support0 libldap-2.4-2 libldap-common libnghttp2-14 libpsl5 libroken18-heimdal
  librtmp1 libsasl2-2 libsasl2-modules libsasl2-modules-db libsqlite3-0 libssh-4 libssl1.1 libwind0-heimdal openssl
  publicsuffix
Suggested packages:
  krb5-doc krb5-user libsasl2-modules-gssapi-mit | libsasl2-modules-gssapi-heimdal libsasl2-modules-ldap
  libsasl2-modules-otp libsasl2-modules-sql
The following NEW packages will be installed:
  ca-certificates curl krb5-locales libasn1-8-heimdal libbrotli1 libcurl4 libgssapi-krb5-2 libgssapi3-heimdal
  libhcrypto4-heimdal libheimbase1-heimdal libheimntlm0-heimdal libhx509-5-heimdal libk5crypto3 libkeyutils1
  libkrb5-26-heimdal libkrb5-3 libkrb5support0 libldap-2.4-2 libldap-common libnghttp2-14 libpsl5 libroken18-heimdal
  librtmp1 libsasl2-2 libsasl2-modules libsasl2-modules-db libsqlite3-0 libssh-4 libssl1.1 libwind0-heimdal openssl
  publicsuffix
0 upgraded, 32 newly installed, 0 to remove and 0 not upgraded.
Need to get 5445 kB of archives.
After this operation, 16.7 MB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 libssl1.1 amd64 1.1.1f-1ubuntu2.4 [1319 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 openssl amd64 1.1.1f-1ubuntu2.4 [620 kB]
Get:3 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 ca-certificates all 20210119~20.04.1 [146 kB]
Get:4 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 libsqlite3-0 amd64 3.31.1-4ubuntu0.2 [549 kB]
Setting up ca-certificates (20210119~20.04.1) ...
debconf: unable to initialize frontend: Dialog
debconf: (No usable dialog-like program is installed, so the dialog based frontend cannot be used. at /usr/share/perl5/Debconf/FrontEnd/Dialog.pm line 76.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (Can't locate Term/ReadLine.pm in @INC (you may need to install the Term::ReadLine module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.30.0 /usr/local/share/perl/5.30.0 /usr/lib/x86_64-linux-gnu/perl5/5.30 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl/5.30 /usr/share/perl/5.30 /usr/local/lib/site_perl /usr/lib/x86_64-linux-gnu/perl-base) at /usr/share/perl5/Debconf/FrontEnd/Readline.pm line 7.)
debconf: falling back to frontend: Teletype
Updating certificates in /etc/ssl/certs...
129 added, 0 removed; done.
Setting up libwind0-heimdal:amd64 (7.7.0+dfsg-1ubuntu1) ...
Setting up libgssapi-krb5-2:amd64 (1.17-6ubuntu4.1) ...
Setting up libssh-4:amd64 (0.9.3-2ubuntu2.1) ...
Setting up libhx509-5-heimdal:amd64 (7.7.0+dfsg-1ubuntu1) ...
Setting up libkrb5-26-heimdal:amd64 (7.7.0+dfsg-1ubuntu1) ...
Setting up libheimntlm0-heimdal:amd64 (7.7.0+dfsg-1ubuntu1) ...
Setting up libgssapi3-heimdal:amd64 (7.7.0+dfsg-1ubuntu1) ...
Setting up libldap-2.4-2:amd64 (2.4.49+dfsg-2ubuntu1.8) ...
Setting up libcurl4:amd64 (7.68.0-1ubuntu2.5) ...
Setting up curl (7.68.0-1ubuntu2.5) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
Processing triggers for ca-certificates (20210119~20.04.1) ...
Updating certificates in /etc/ssl/certs...
0 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
root@014072c1cd18:~# curl -s https://raw.githubusercontent.com/datacharmer/dbdeployer/master/scripts/dbdeployer-install.sh | bash
tool 'shasum' not found
root@014072c1cd18:~#
ha-perl root@014072c1cd18:~#     apt-get install libdigest-sha-perl
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  libgdbm-compat4 libgdbm6 libperl5.30 netbase perl perl-modules-5.30
Suggested packages:
  gdbm-l10n perl-doc libterm-readline-gnu-perl | libterm-readline-perl-perl make libb-debug-perl liblocale-codes-perl
The following NEW packages will be installed:
  libdigest-sha-perl libgdbm-compat4 libgdbm6 libperl5.30 netbase perl perl-modules-5.30
0 upgraded, 7 newly installed, 0 to remove and 0 not upgraded.
Need to get 7004 kB of archives.
After this operation, 46.4 MB of additional disk space will be used.
Do you want to continue? [Y/n]
Get:1 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 perl-modules-5.30 all 5.30.0-9ubuntu0.2 [2738 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal/main amd64 libgdbm6 amd64 1.18.1-5 [27.4 kB]
Get:3 http://archive.ubuntu.com/ubuntu focal/main amd64 libgdbm-compat4 amd64 1.18.1-5 [6244 B]
Get:4 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 libperl5.30 amd64 5.30.0-9ubuntu0.2 [3952 kB]
Get:5 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 perl amd64 5.30.0-9ubuntu0.2 [224 kB]
Get:6 http://archive.ubuntu.com/ubuntu focal/main amd64 netbase all 6.1 [13.1 kB]
Get:7 http://archive.ubuntu.com/ubuntu focal/universe amd64 libdigest-sha-perl amd64 6.02-1build2 [43.8 kB]
Fetched 7004 kB in 4s (1947 kB/s)
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package perl-modules-5.30.
(Reading database ... 4661 files and directories currently installed.)
Preparing to unpack .../0-perl-modules-5.30_5.30.0-9ubuntu0.2_all.deb ...
Unpacking perl-modules-5.30 (5.30.0-9ubuntu0.2) ...
Selecting previously unselected package libgdbm6:amd64.
Preparing to unpack .../1-libgdbm6_1.18.1-5_amd64.deb ...
Unpacking libgdbm6:amd64 (1.18.1-5) ...
Selecting previously unselected package libgdbm-compat4:amd64.
Preparing to unpack .../2-libgdbm-compat4_1.18.1-5_amd64.deb ...
Unpacking libgdbm-compat4:amd64 (1.18.1-5) ...
Selecting previously unselected package libperl5.30:amd64.
Preparing to unpack .../3-libperl5.30_5.30.0-9ubuntu0.2_amd64.deb ...
Unpacking libperl5.30:amd64 (5.30.0-9ubuntu0.2) ...
Selecting previously unselected package perl.
Preparing to unpack .../4-perl_5.30.0-9ubuntu0.2_amd64.deb ...
Unpacking perl (5.30.0-9ubuntu0.2) ...
Selecting previously unselected package netbase.
Preparing to unpack .../5-netbase_6.1_all.deb ...
Unpacking netbase (6.1) ...
Selecting previously unselected package libdigest-sha-perl.
Preparing to unpack .../6-libdigest-sha-perl_6.02-1build2_amd64.deb ...
Adding 'diversion of /usr/bin/shasum to /usr/bin/shasum.bundled by libdigest-sha-perl'
Adding 'diversion of /usr/share/man/man1/shasum.1.gz to /usr/share/man/man1/shasum.bundled.1.gz by libdigest-sha-perl'
Unpacking libdigest-sha-perl (6.02-1build2) ...
Setting up perl-modules-5.30 (5.30.0-9ubuntu0.2) ...
Setting up netbase (6.1) ...
Setting up libgdbm6:amd64 (1.18.1-5) ...
Setting up libgdbm-compat4:amd64 (1.18.1-5) ...
Setting up libperl5.30:amd64 (5.30.0-9ubuntu0.2) ...
Setting up perl (5.30.0-9ubuntu0.2) ...
Setting up libdigest-sha-perl (6.02-1build2) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
root@014072c1cd18:~# curl -s https://raw.githubusercontent.com/datacharmer/dbdeployer/master/scripts/dbdeployer-install.sh | bash
dbdeployer-1.62.0.linux.tar.gz: OK
-rwxr-xr-x 1  501 staff  12M Jun  6 08:52 dbdeployer-1.62.0.linux
-rw-r--r-- 1 root root  5.9M Jul  6 09:46 dbdeployer-1.62.0.linux.tar.gz
-rw-r--r-- 1 root root    97 Jul  6 09:46 dbdeployer-1.62.0.linux.tar.gz.sha256
/usr/local/bin
/usr/bin
File dbdeployer-1.62.0.linux copied to /usr/local/bin/dbdeployer
root@014072c1cd18:~# dbdeployer init
SANDBOX_BINARY /root/opt/mysql
SANDBOX_HOME   /root/sandboxes

--------------------------------------------------------------------------------
Directory /root/opt/mysql ($SANDBOX_BINARY) was created
This directory is the destination for expanded tarballs

--------------------------------------------------------------------------------
Directory /root/sandboxes ($SANDBOX_HOME) was created
This directory is the destination for deployed sandboxes

--------------------------------------------------------------------------------
# dbdeployer downloads get mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz
....  51 MB
--------------------------------------------------------------------------------
# dbdeployer unpack mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz
Unpacking tarball mysql-8.0.25-linux-glibc2.17-x86_64-minimal.tar.xz to $HOME/opt/mysql/8.0.25
Renaming directory /root/opt/mysql/mysql-8.0.25-linux-glibc2.17-x86_64-minimal to /root/opt/mysql/8.0.25
--------------------------------------------------------------------------------
dbdeployer versions
Basedir: /root/opt/mysql
8.0.25
--------------------------------------------------------------------------------
# dbdeployer defaults enable-bash-completion --run-it --remote
Error: neither /etc/bash_completion.d or /usr/local/etc/bash_completion.d found
Usage:
  dbdeployer init [flags]

Flags:
      --dry-run                 Show operations but don't run them
  -h, --help                    help for init
      --skip-all-downloads      Do not download any file (skip both MySQL tarball and shell completion file)
      --skip-shell-completion   Do not download shell completion file
      --skip-tarball-download   Do not download MySQL tarball

Global Flags:
      --config string           configuration file (default "/root/.dbdeployer/config.json")
      --sandbox-binary string   Binary repository (default "/root/opt/mysql")
      --sandbox-home string     Sandbox deployment directory (default "/root/sandboxes")
      --shell-path string       Path to Bash, used for generated scripts (default "/usr/bin/bash")
The following additional packages will be installed:
  libgpm2 libtinfo5
Suggested packages:
  gpm
The following NEW packages will be installed:
  libgpm2 libncurses5 libtinfo5
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 195 kB of archives.
After this operation, 925 kB of additional disk space will be used.
Do you want to continue? [Y/n]
Get:1 http://archive.ubuntu.com/ubuntu focal/main amd64 libgpm2 amd64 1.20.7-5 [15.1 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal/universe amd64 libtinfo5 amd64 6.2-0ubuntu2 [83.0 kB]
Get:3 http://archive.ubuntu.com/ubuntu focal/universe amd64 libncurses5 amd64 6.2-0ubuntu2 [96.9 kB]
Fetched 195 kB in 1s (138 kB/s)
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package libgpm2:amd64.
(Reading database ... 6633 files and directories currently installed.)
Preparing to unpack .../libgpm2_1.20.7-5_amd64.deb ...
Unpacking libgpm2:amd64 (1.20.7-5) ...
error while filling the sandbox definition: missing libraries will prevent MySQL from deploying correctly

server (/root/opt/mysql/8.0.25/bin/mysqld): [   libaio.so.1 => not found        libnuma.so.1 => not found]
global: [libaio libnuma]
.
Use --skip-library-check to skip this check
root@014072c1cd18:~# apt install libaio1 libnuma1
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  libaio1 libnuma1
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 28.0 kB of archives.
After this operation, 110 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu focal/main amd64 libnuma1 amd64 2.0.12-1 [20.8 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal/main amd64 libaio1 amd64 0.3.112-5 [7184 B]
Fetched 28.0 kB in 1s (19.6 kB/s)
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package libnuma1:amd64.
(Reading database ... 6653 files and directories currently installed.)
Preparing to unpack .../libnuma1_2.0.12-1_amd64.deb ...
Unpacking libnuma1:amd64 (2.0.12-1) ...
Selecting previously unselected package libaio1:amd64.
Preparing to unpack .../libaio1_0.3.112-5_amd64.deb ...
Unpacking libaio1:amd64 (0.3.112-5) ...
Setting up libnuma1:amd64 (2.0.12-1) ...
Setting up libaio1:amd64 (0.3.112-5) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
root@014072c1cd18:~# dbdeployer deploy single 8.0.25
Database installed in $HOME/sandboxes/msb_8_0_25
run 'dbdeployer usage single' for basic instructions'
. sandbox server started
root@014072c1cd18:~# cd sandboxes/msb_8_0_25/
root@014072c1cd18:~/sandboxes/msb_8_0_25# ./use --user=root mysql
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 8.0.25 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql [localhost:8025] {root} (mysql) >
```

Final solution is to run the sandboxes in their own LXC on my local proxmox.