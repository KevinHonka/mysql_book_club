# Platforms

MySQL is available on:

- Linux
- IBM AIX
- FreeBSD
- Windows
- macOS

[Site for supported platforms](https://www.mysql.com/support/supportedplatforms/database.html)  

32-bit support only on older OS Versions like Ubuntu 18.04 LTS.  
ARM64 Support on Oracle Linux 8, CentOS/RHEL 8 as well as Oracle Linux 7, CentOS/RHEL 7.  

# Versions

MySQL follows the [Apache Versioning](https://commons.apache.org/releases/versioning.html) for their version numbers.  
There are also suffixes to indicate what stability is to be expected from version. 

- **dmr**: development milestone, highly experimental stuff
- **rc**: release candidate, can be tested
- **ga**: stable release, which can be run in prod

# Installation

There are seperate MySQL repositories which carry much newer versions to those that are in the OS repositories and should be used.

- [Yum Repo](https://dev.mysql.com/downloads/repo/yum/)
- [APT Repo](https://dev.mysql.com/downloads/repo/apt/)
- [SLES Repo](https://dev.mysql.com/downloads/repo/suse/)

## MySQL on Windows

Oracle recommends to use the [MSI Installer](https://dev.mysql.com/downloads/installer/).  
Recommendation to install MySQL with admin rights?!  
Special care if tables become larger than 4GB is needed.  
MySQL needs NTFS to work properly.  

Does it work on WSL2? gonna have to check.

## MySQL on macOS

Oracle recommends using native packages. No idea, how that works.

## MySQL on Linux

Many repositories are available for RPM, DEB, Generic packages.  
What is Oracle Unbreakable Linux Network(ULN)?  
Seems to be similar to a Red Hat Repository, but from Oracle.

## MySQL on Solaris/FreeBSD

Both kind of exotic and not really in my scope.

# Postinstallation

Seems similar to MariaDB.  
MySQL autogenerates a root pw when run with `--initialize` using `--initialize-insecure` allows root access without a password.  
